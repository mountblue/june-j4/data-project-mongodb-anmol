let keys = [];
let values = [];
let index = 0;
for (var key in matchData) {
  keys[index] = key;
  values[index] = matchData[key];
  index++;
}

Highcharts.chart('high-chart', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'IPL Data'
  },
  subtitle: {
    text: 'Local source'
  },
  xAxis: {
    categories: keys,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Matches'
    }
  },
  tooltip: {},
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'The number of matches played per year of all the years in IPL.',
    data: values

  }]
});