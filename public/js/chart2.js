let allSeason = [];
for (i of matchData) {
    for (j of i.season) {
        if (!allSeason.includes(j)) {
            allSeason.push(j);
        }
    }
}
allSeason.sort();
let mainData = [];
for (i in matchData) {
    if (matchData[i]._id !== "") {
        let arr = [];
        let k = 0;
        for (j in allSeason) {
            if (allSeason[j] === matchData[i].season[k]) {
                arr[j] = matchData[i].wins[k];
                k++;
            } else {
                arr[j] = 0;
            }
        }

        let obj = {
            name: matchData[i]._id,
            data: arr
        }
        mainData.push(obj);
        obj = {};
    }
}
console.log(mainData);
Highcharts.chart('high-chart', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Stacked bar chart Of IPL data'
    },
    xAxis: {
        categories: allSeason
    },
    yAxis: {
        min: 0,
        title: {
            text: 'IPL all seasons'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: mainData
});