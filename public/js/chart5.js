let keys = [];
let values = [];
let index = 0;
for (var key in matchData) {
  keys[index] = key;
  values[index] = matchData[key];
  index++;
  if (index > 9) {
    break;
  }
}

Highcharts.chart('high-chart', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'IPL Data'
  },
  subtitle: {
    text: 'Local source'
  },
  xAxis: {
    categories: keys,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Total Runs'
    }
  },
  tooltip: {},
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'For the year 2016, the top batsman with highest score.',
    data: values

  }]
});