const expect = require("chai").expect,
    path = require("path"),
    filePath = path.resolve("chartFunctions.js"),
    fileName = require(filePath),
    dbName = "demo-datadb";


describe("The number of matches played per year of all the years in IPL.", function () {
    it("getMatchesPerYear function should not be undefined and null", function () {
        expect(fileName.getMatchesPerYear(dbName)).to.exist;
    })

    it("should hava a database connection build", async function () {
        let output = await fileName.getMatchesPerYear(dbName)
        expect(output).to.throw(Error);
    })

    it("should return the accepted result", async function () {
        let expectedResult = {
            2015: 3,
            2016: 4
        }
        let output = await fileName.getMatchesPerYear(dbName);
        expect(output).to.deep.equal(expectedResult);
    })
})

////second question pending

describe("Matches won of all teams over all the years of IPL.", function () {
    it("getMatchesWonPerTeam function should not be undefined and null", function () {
        expect(fileName.getMatchesWonPerTeam(dbName)).to.exist;
    })

    it("should hava a database connection build", async function () {
        let output = await fileName.getMatchesWonPerTeam(dbName);
        expect(output).to.throw(Error);
    })

    it("should return the accepted result", async function () {
        let expectedResult = [{
                _id: 'Rising Pune Supergiants',
                wins: [1],
                season: [2016]
            },
            {
                _id: 'Chennai Super Kings',
                wins: [1],
                season: [2015]
            },
            {
                _id: 'Kolkata Knight Riders',
                wins: [1],
                season: [2016]
            },
            {
                _id: 'Mumbai Indians',
                wins: [1],
                season: [2015]
            },
            {
                _id: 'Royal Challengers Bangalore',
                wins: [1, 1],
                season: [2015, 2016]
            },
            {
                _id: 'Gujarat Lions',
                wins: [1],
                season: [2016]
            }
        ]


        let output = await fileName.getMatchesWonPerTeam(dbName);
        expect(output).to.deep.equal(expectedResult);
    })
})
////
describe("The year 2016, the extra runs conceded per team.", function () {
    it("getExtraRunsPerTeam function should not be undefined and null", function () {
        expect(fileName.getExtraRunsPerTeam(dbName)).to.exist;
    })

    it("should hava a database connection build", async function () {
        let output = await fileName.getExtraRunsPerTeam(dbName);
        expect(output).to.throw(Error);
    })

    it("should return the accepted result", async function () {
        let expectedResult = {
            "Rising Pune Supergiants": 0,
            "Royal Challengers Bangalore": 5,
            "Mumbai Indians": 0,
            "Kolkata Knight Riders": 0,
            "Sunrisers Hyderabad": 0,
            "Kings XI Punjab": 0,
            "Delhi Daredevils": 0,
            "Gujarat Lions": 0
        }

        let output = await fileName.getExtraRunsPerTeam(dbName);
        expect(output).to.deep.equal(expectedResult);
    })
})

describe("The year 2015, the top economical bowlers.", function () {
    it("getEconomicalBowlers function should not be undefined and null", function () {
        expect(fileName.getEconomicalBowlers(dbName)).to.exist;
    })

    it("should hava a database connection build", async function () {
        let output = await fileName.getEconomicalBowlers(dbName);
        expect(output).to.throw(Error);
    })

    it("should return the accepted result", async function () {
        let expectedResult = {
            "A Nehra": 7,
            "CH Morris": 8,
            "MA Starc": 6,
            "SL Malinga": 0
        }

        let output = await fileName.getEconomicalBowlers(dbName);
        expect(output).to.deep.equal(expectedResult);
    })
})

describe("The year 2016, the top batsman with highest score.", function () {
    it("getTopBatsman function should not be undefined and null", function () {
        expect(fileName.getTopBatsman(dbName)).to.exist;
    })

    it("should hava a database connection build", async function () {
        let output = await fileName.getTopBatsman(dbName);
        expect(output).to.throw(Error);
    })

    it("should return the accepted result", async function () {
        let expectedResult = {
            "AJ Finch": 0,
            "AM Rahane": 4,
            "CH Gayle": 1,
            "DA Warner": 1,
            "LMP Simmons": 1,
            "M Vijay": 1,
            "M Vohra": 0,
            "MA Agarwal": 4,
            "RG Sharma": 0,
            "RV Uthappa": 0,
        }

        let output = await fileName.getTopBatsman(dbName);
        expect(output).to.deep.equal(expectedResult);
    })
})