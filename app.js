const express = require("express"),
    app = express(),
    http = require("http"),
    server = http.Server(app),
    url = "mongo://126.0.0.1:27017",
    bodyParser = require("body-parser"),
    path = require("path"),
    filePath = path.resolve("chartFunctions.js"),
    fileName = require(filePath),
    dbName = "datadb";

app.use(express.static("public/js"));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.set("view engine", "ejs");

fileName.getMatchesPerYear(dbName).then(function (data) {
    app.get('/chart1', function (req, res) {
        res.render('hc1', {
            data: JSON.stringify(data)
        });
    });

})

fileName.getMatchesWonPerTeam(dbName).then(function (data) {
    app.get('/chart2', function (req, res) {
        res.render('hc2', {
            data: JSON.stringify(data)
        });
    });

})

fileName.getExtraRunsPerTeam(dbName).then(function (data) {
    app.get('/chart3', function (req, res) {
        res.render('hc3', {
            data: JSON.stringify(data)
        });
    });

})

fileName.getEconomicalBowlers(dbName).then(function (data) {
    app.get('/chart4', function (req, res) {
        res.render('hc4', {
            data: JSON.stringify(data)
        });
    });

})

fileName.getTopBatsman(dbName).then(function (data) {
    app.get('/chart5', function (req, res) {
        res.render('hc5', {
            data: JSON.stringify(data)
        });
    });

})

//port, ip, callback parameters that can be passed in listen function
app.listen('5000', 'localhost', function () {
    console.log("Started the server.");
});