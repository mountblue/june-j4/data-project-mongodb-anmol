const MongoClient = require("mongodb").MongoClient,
    url = "mongodb://127.0.0.1:27017";

let getMatchesPerYear = function (dbName) {

    return new Promise(function (resolve, reject) {
        let matchesPerYear = {};

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                "$group": {
                    "_id": "$season",
                    "count": {
                        "$sum": 1
                    }
                }
            }, {
                "$sort": {
                    _id: 1
                }
            }]).toArray(function (err, res) {
                for (var key in res) {
                    matchesPerYear[res[key]._id] = res[key].count;

                }
                resolve(matchesPerYear);
            })

        })

    })
}

let getMatchesWonPerTeam = function (dbName) {
    return new Promise(function (resolve, reject) {
        let matchesWonPerTeam = {};
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                    $group: {
                        _id: {
                            season: "$season",
                            teams: "$winner"
                        },
                        wins: {
                            $sum: 1
                        }
                    }
                }, {
                    $sort: {
                        _id: 1
                    }
                },
                {
                    $group: {
                        _id: "$_id.teams",
                        wins: {
                            $push: "$wins"
                        },
                        season: {
                            $push: "$_id.season"
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        wins: 1,
                        season: 1
                    }
                }
            ]).toArray(function (err, res) {
                resolve(res);
            })

        })


    })
}

let getExtraRunsPerTeam = function (dbName) {
    return new Promise(function (resolve, reject) {
        let extraRunsPerTeam = {};
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                "$match": {
                    season: 2016
                }
            }, {
                $sort: {
                    _id: 1
                }
            }, {
                $lookup: {
                    from: "deliveries",
                    localField: "id",
                    foreignField: "match_id",
                    as: "deliveries_docs"
                }
            }, {
                $unwind: "$deliveries_docs"
            }, {
                $group: {
                    _id: "$deliveries_docs.bowling_team",
                    extraRuns: {
                        $sum: "$deliveries_docs.extra_runs"
                    }
                }
            }]).toArray(function (err, res) {
                for (var key in res) {
                    extraRunsPerTeam[res[key]._id] = res[key].extraRuns;

                }
                resolve(extraRunsPerTeam);
            })

        })

    })
}

let getEconomicalBowlers = function (dbName) {
    return new Promise(function (resolve, reject) {
        let economicalBowlers = {};

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                "$match": {
                    season: 2015
                }
            }, {
                $sort: {
                    _id: 1
                }
            }, {
                $lookup: {
                    from: "deliveries",
                    localField: "id",
                    foreignField: "match_id",
                    as: "deliveries_docs"
                }
            }, {
                $unwind: "$deliveries_docs"
            }, {
                $group: {
                    _id: "$deliveries_docs.bowler",
                    totalRuns: {
                        $sum: "$deliveries_docs.total_runs"
                    },
                    totalBalls: {
                        $sum: "$deliveries_docs.ball"
                    }
                }
            }, {
                $project: {
                    economy: {
                        $multiply: [{
                            $divide: ["$totalRuns", "$totalBalls"]
                        }, 6]
                    }
                }
            }, {
                $sort: {
                    economy: 1
                }
            }]).toArray(function (err, res) {
                for (var key in res) {
                    economicalBowlers[res[key]._id] = res[key].economy;

                }
                resolve(economicalBowlers);
            })
        })
    })
}

let getTopBatsman = function (dbName) {
    return new Promise(function (resolve, reject) {
        let topBatsman = {};

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log("Database not found. " + err);
                resolve(err);
            } else {
                console.log("connected database");
            }
            let dbObject = conn.db(dbName);
            let collection = dbObject.collection("matches");
            collection.aggregate([{
                "$match": {
                    season: 2016
                }
            }, {
                $sort: {
                    _id: 1
                }
            }, {
                $lookup: {
                    from: "deliveries",
                    localField: "id",
                    foreignField: "match_id",
                    as: "deliveries_docs"
                }
            }, {
                $unwind: "$deliveries_docs"
            }, {
                $group: {
                    _id: "$deliveries_docs.batsman",
                    totalRuns: {
                        $sum: "$deliveries_docs.batsman_runs"
                    }
                }
            }, {
                $sort: {
                    totalRuns: -1
                }
            }]).toArray(function (err, res) {
                for (var key in res) {
                    topBatsman[res[key]._id] = res[key].totalRuns;

                }
                resolve(topBatsman);
            })
        })

    })
}

module.exports = {
    getMatchesPerYear,
    getMatchesWonPerTeam,
    getExtraRunsPerTeam,
    getEconomicalBowlers,
    getTopBatsman
}